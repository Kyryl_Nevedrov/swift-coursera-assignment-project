//
//  ViewController.swift
//  AppleCoursera
//
//  Created by Admin on 06.07.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    var productTitle: String = ""
    var productDescription: String = ""
    var productImage: UIImage = UIImage()
   
      
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var zoomTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet weak var productDescriptionTextView: UITextView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleNavigationItem: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        zoomTapGestureRecognizer.numberOfTapsRequired = 2
        productImageView.image = productImage
        productDescriptionTextView.text = productDescription
        productTitleNavigationItem.title = productTitle
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.productImageView
    }

    @IBAction func tapForDefaultZoom(sender: UITapGestureRecognizer) {
        self.scrollView.setZoomScale(1.0, animated: true)
    }
  

}

