//
//  AddItemViewController.swift
//  AppleCoursera
//
//  Created by Admin on 11.07.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var productLines: [ProductLine] = []
    
    lazy var sessionConfiguration: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
    lazy var session: NSURLSession = NSURLSession(configuration: self.sessionConfiguration)
    
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var imageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveItemForTableView(sender: UIBarButtonItem) {
        let validate: (Bool, String) = validation()
        if validate.0 {
            productLines[pickerView.selectedRowInComponent(0)].products.append(Product(titled: titleTextField.text!, description: descriptionTextField.text!, image: imageView.image!))
            self.performSegueWithIdentifier("unwind", sender: self)
           
            
        } else {
            alertShow(validate.1)
        }
        
    }
    
    func validation() -> (Bool, String) {
        var validate: Bool = true
        var errorString = ""
        if titleTextField.text?.characters.count < 3 {
            validate =  false
            errorString += "title lenght must be greater than 3"
        }
        if descriptionTextField.text?.characters.count < 10 {
            validate = false
            errorString += "description lenght must be greater than 10"
        }
        return (validate, errorString)
    }
    
    func downloadImage(completion: (NSData -> Void)) {
        
        let urlString = self.urlTextField.text!
        if let url = NSURL(string: urlString) {
        
            let request = NSURLRequest(URL: url)
            let dataTask = session.dataTaskWithRequest(request) { (data, response, error) in
                if error == nil {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        switch httpResponse.statusCode {
                        case 200:
                            if let data = data {
                                completion(data)
                            }
                        default:
                            print(httpResponse.statusCode)
                        }
                    }
                
                } else {
                    self.alertShow(error!.localizedDescription)
                }
            }
            dataTask.resume()
        } else {
            alertShow("Incorect url")
        }
        
        
    }
    
    func alertShow(error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .Alert)
        let alertAction = UIAlertAction(title: "Error", style: .Default, handler: nil)
        alert.addAction(alertAction)
        presentViewController(alert, animated: true, completion: nil)
        
    }
    
   
    
    @IBAction func downloadImageButton(sender: UIButton) {
        downloadImage { (data) in
            let image = UIImage(data: data)
            dispatch_async(dispatch_get_main_queue(), { 
                self.imageView.image = image
            })
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return productLines.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return productLines[row].name
    }

   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destinationController = segue.destinationViewController as? AppleTableTableViewController
        destinationController?.productLines = productLines
        print("tom")
    }
 

}
